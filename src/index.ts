window.addEventListener('load', async () => {
    const HTMLhref = new URL('./app/app.html', window.location.href).href;
    const HTMLFetching = await fetch(HTMLhref).then((resp) => resp.text());

    const root = document.getElementById('root');

    root.innerHTML = HTMLFetching;

    await getStyles();
    await getScripts();
});

/**
 * @description Get project scripts
 * @return {Promise<void>}
 */
const getScripts = async () => {
    const scriptsPath: string[] = [
        new URL('./app/app.js', window.location.href).href,
        new URL('./app/components/authModal/auth.js', window.location.href).href,
    ];

    const scriptsArray: string[] = [];

    for (const path of scriptsPath) {
        const styleFetch = await fetch(path).then((resp) => resp.text());
        scriptsArray.push(styleFetch);
    }
    const scripts = scriptsArray.join('');

    const html = document.getElementsByTagName('html')[0];
    const script = document.createElement('script');

    script.appendChild(document.createTextNode(scripts));

    html.appendChild(script);
};

/**
 * @description Get project styles
 * @return {Promise<void>}
 */
const getStyles = async () => {
    const stylesPath: string[] = [
        new URL('./app/app.css', window.location.href).href,
        new URL('./app/components/authModal/auth.css', window.location.href).href,
    ];

    const stylesArray: string[] = [];

    for (const path of stylesPath) {
        const styleFetch = await fetch(path).then((resp) => resp.text());
        stylesArray.push(styleFetch);
    }
    const styles = stylesArray.join('');

    const head = document.head;
    const style = document.createElement('style');

    style.appendChild(document.createTextNode(styles));

    head.appendChild(style);
};
