var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
window.addEventListener('load', function () { return __awaiter(_this, void 0, void 0, function () {
    var HTMLhref, HTMLFetching, root;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                HTMLhref = new URL('./app/app.html', window.location.href).href;
                return [4 /*yield*/, fetch(HTMLhref).then(function (resp) { return resp.text(); })];
            case 1:
                HTMLFetching = _a.sent();
                root = document.getElementById('root');
                root.innerHTML = HTMLFetching;
                return [4 /*yield*/, getStyles()];
            case 2:
                _a.sent();
                return [4 /*yield*/, getScripts()];
            case 3:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); });
/**
 * @description Get project scripts
 * @return {Promise<void>}
 */
var getScripts = function () { return __awaiter(_this, void 0, void 0, function () {
    var scriptsPath, scriptsArray, _i, scriptsPath_1, path, styleFetch, scripts, html, script;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                scriptsPath = [
                    new URL('./app/app.js', window.location.href).href,
                    new URL('./app/components/authModal/auth.js', window.location.href).href,
                ];
                scriptsArray = [];
                _i = 0, scriptsPath_1 = scriptsPath;
                _a.label = 1;
            case 1:
                if (!(_i < scriptsPath_1.length)) return [3 /*break*/, 4];
                path = scriptsPath_1[_i];
                return [4 /*yield*/, fetch(path).then(function (resp) { return resp.text(); })];
            case 2:
                styleFetch = _a.sent();
                scriptsArray.push(styleFetch);
                _a.label = 3;
            case 3:
                _i++;
                return [3 /*break*/, 1];
            case 4:
                scripts = scriptsArray.join('');
                html = document.getElementsByTagName('html')[0];
                script = document.createElement('script');
                script.appendChild(document.createTextNode(scripts));
                html.appendChild(script);
                return [2 /*return*/];
        }
    });
}); };
/**
 * @description Get project styles
 * @return {Promise<void>}
 */
var getStyles = function () { return __awaiter(_this, void 0, void 0, function () {
    var stylesPath, stylesArray, _i, stylesPath_1, path, styleFetch, styles, head, style;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                stylesPath = [
                    new URL('./app/app.css', window.location.href).href,
                    new URL('./app/components/authModal/auth.css', window.location.href).href,
                ];
                stylesArray = [];
                _i = 0, stylesPath_1 = stylesPath;
                _a.label = 1;
            case 1:
                if (!(_i < stylesPath_1.length)) return [3 /*break*/, 4];
                path = stylesPath_1[_i];
                return [4 /*yield*/, fetch(path).then(function (resp) { return resp.text(); })];
            case 2:
                styleFetch = _a.sent();
                stylesArray.push(styleFetch);
                _a.label = 3;
            case 3:
                _i++;
                return [3 /*break*/, 1];
            case 4:
                styles = stylesArray.join('');
                head = document.head;
                style = document.createElement('style');
                style.appendChild(document.createTextNode(styles));
                head.appendChild(style);
                return [2 /*return*/];
        }
    });
}); };
